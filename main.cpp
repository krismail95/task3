#include <iostream>
#include <oop.h>
using namespace std;
int main() {
cout << "* Create object Base" << endl;
Base base;
cout << "* Create object Child" << endl;
Child child;
cout << "* Call method func() for object Base" << endl;
Function(base);
cout << "* Call method func() for object Child" << endl;
Function(child);
return 0;
}
